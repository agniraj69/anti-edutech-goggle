# anti-edutech-goggle

A simple Brave Goggle for removing edutech links from brave searches. Also my first project :) 

## Description

A simple Brave Goggle that removes annoying edutech links that spams search results. This offcourse doesn't remove useful sites like khanacademy.org that truly help with a subject rather than selling a course. Currently added websites are,

* byjus.com
* toppr.com
* unacademy.com
* vedantu.com
* embibe.com
* askiitians.com
* cuemath.com
* brainly.in
* doubtnut.com
* sarthaks.com
* studyboard.com
* tardigrade.in
* infinitylearn.com
* shaalaa.com
* testbook.com

And many more to come.

## To-do

- [ ] Filter YouTube results from these companies and more.
- [ ] Make the code more efficient if possible


